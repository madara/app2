@extends('user.master')
@section('content')
<div class="container container-alt">
                    <div class="card" id="profile-main" >

                        <div class="pm-body clearfix">                         


                           <div class="pmb-block ">
                                <div class="pmbb-header">
                                    <h2><i class="zmdi zmdi-account m-r-10"></i> Basic Information</h2>

                                    <ul class="actions">
                                        <li class="dropdown">
                                            <a href="" data-toggle="dropdown">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a data-ma-action="profile-edit" href="">Edit</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="pmbb-body p-l-30">

                                    <div class="pmbb-view">
                                        <dl class="dl-horizontal">
                                            <dt>First Name</dt>
                                            <dd>{{$user->first_name}}</dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Second Name</dt>
                                            <dd>{{$user->second_name}}</dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Username</dt>
                                            <dd>{{$user->username}}</dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Gender</dt>
                                            <dd>{{$user->gender}}</dd>
                                        </dl> 
                                         <dl class="dl-horizontal">
                                            <dt>ID Number</dt>
                                            <dd>{{$user->id_no}}</dd>
                                        </dl>                                      
                                    </div>
                                    <form method="POST" action="{{route('updateBasic')}}">
                                    {{csrf_field()}}
                                    <div class="pmbb-edit">
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">First Name</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="first_name" class="form-control"
                                                         value="{{$user->first_name}}">
                                                </div>

                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Second Name</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="second_name" class="form-control"
                                                         value="{{$user->second_name}}">
                                                </div>

                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Username</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" class="form-control" name="username" 
                                                         value="{{$user->username}}">
                                                </div>

                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Gender</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <select class="form-control" name="gender">
                                                    @if($user->gender=='Male')
                                                        <option selected="selected" value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                               		@elseif($user->gender=='Female')
                                                        <option selected="selected" value="Female">Female</option>
                                                        <option value="Male"> Male</option>

                                                        @elseif($user->gender==null)
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        @endif

                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">ID Number</dt>
                                            <dd>
                                                <div class="fg-line input-mask">
                                                    <input type="text" class="form-control" name="id_no"  data-mask="00000000"
                                                         value="{{$user->id_no}}">
                                                </div>

                                            </dd>
                                        </dl>
                                        

                                        <div class="m-t-30">
                                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                            <button data-ma-action="profile-edit-cancel" class="btn btn-link btn-sm">Cancel</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <form method="POST" action="{{route('updateContact')}}">
                            {{csrf_field()}}
                            <div class="pmb-block">
                                <div class="pmbb-header">
                                    <h2><i class="zmdi zmdi-phone m-r-10"></i> Contact Information</h2>

                                    <ul class="actions">
                                        <li class="dropdown">
                                            <a href="" data-toggle="dropdown">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a data-ma-action="profile-edit" href="">Edit</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="pmbb-body p-l-30">
                                    <div class="pmbb-view">
                                        <dl class="dl-horizontal">
                                            <dt>Mobile Phone</dt>
                                            <dd>{{$user->phone}}</dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Email Address</dt>
                                            <dd>{{$user->email}}</dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt>Location</dt>
                                            <dd>{{$user->location}}</dd>
                                        </dl>
                                        
                                    </div>

                                    <div class="pmbb-edit">
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Mobile Phone</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" class="form-control" name="phone" 
                                                           value="{{$user->phone}}">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Email Address</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="email" class="form-control" name="email" 
                                                           value="{{$user->email}}">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Location</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" class="form-control" name="location" 
                                                           value="{{$user->location}}">
                                                </div>
                                            </dd>
                                        </dl>
                                        

                                        <div class="m-t-30">
                                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                            <button data-ma-action="profile-edit-cancel" class="btn btn-link btn-sm">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
 </div>
@endsection