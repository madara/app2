@extends('user.master')
@section('content')
<div class="card">
	<div class="card-header">
		<h2>Payment Procedure</h2>
	</div>
	<div class="card-body card-padding">
		<div class="row">
			<div class="col-lg-7 m-b-25 m-r-25 f-16">
				 <ol>
				      <li>Lorem ipsum dolor sit amet</li>
				      <li>Consectetur adipiscing elit</li>
				      <li>Integer molestie lorem at massa</li>
				      <li>Facilisis in pretium nisl aliquet</li>
				      <li>Nulla volutpat aliquam velit</li>
				      <li>Faucibus porta lacus fringilla vel</li>

				 </ol>              
           </div>

		</div>
		<button class="btn bgm-bluegray" id="save"><i class=""></i> Save Payment</button>
	</div>
</div>
<div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Subcription Payment</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- content goes here -->
									      <form action="{{route('payment')}}" method="POST">
									      {{csrf_field()}}       
									       <div class="form-group">
									                <label for="title">Amount Paid</label>
									                <input type="text" class="form-control input-mask" data-mask="0000.00" name="amount" id="amount" placeholder="Amount Paid">
									        </div>              
									         <div class="form-group">
									                <label for="title">Days</label>
									                <select name="days" class="form-control selectpicker" required>
									                	<option value="" selected="selected">------------</option>
									                	<option value="30">30</option>
									                	<option value="60">60</option>
									                	<option value="90">90</option>
									                </select>
									         </div>  
									         <div class="form-group">
									                <label for="title">MPESA Receipt</label>
									                <input type="text" class="form-control" name="receipt" id="receipt" required placeholder="MPESA Transaction Receipt">
									        </div> 								         


									        <div class="modal-footer">
									                         
									                    <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Add</button>
									               
									                    <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
									        
									        </div>
									        </form>
                                        </div>
                                       
                                    </div>
                                </div>
 </div>


@endsection
 @section('scripts')

 @endsection