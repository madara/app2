@extends('consultant.master')
@section('content')

                    <div class="card">
                        <div class="card-header">
                            <h2>Clients
                                <small>Clients and their location
                                </small>
                            </h2>
                            @if(Session::has('message'))
                            <p class="alert alert-danger">{{ Session::get('message') }}</p>
                            @endif
                        </div>

                        <table id="data-table-client" class="table table-striped table-vmiddle">
                            <thead>
                            <tr>
                                <th data-column-id="id" data-type="numeric" data-order="asc">#</th>
                                <th data-column-id="name" >Name</th>
                                <th data-column-id="email" data-order="desc">Email</th>
                                <th data-column-id="phone" data-order="desc">Phone</th>
                                <th data-column-id="location" data-order="desc">Location</th> 
                               
                             
                                <th data-column-id="commands"  data-sortable="false" >Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>                          
                            @forelse($clients as $key=>$client)                           
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$client->first_name}}{{' '}}{{$client->second_name}}</td>
                                <td>{{$client->email}}</td>
                                <td>{{$client->phone}}</td>                                
                                <td>{{$client->location}}</td>                               
                               
                                <td>                                
                                <button style="color: #00BCD4" type="button" onclick="#" class="btn btn-icon command-edit waves-effect waves-circle " ><span class="zmdi zmdi-eye" ></span></button>
                                    <button style="color: #00BCD4" type="button" onclick="#" class="btn btn-icon command-edit waves-effect waves-circle " ><span class="zmdi zmdi-email" ></span></button>
                                </td>
                            </tr> 
                            @empty
                            <tr>
                            <td colspan="9" style="text-align: center; color: #03A9F4;">No New Clients at the moment</td></tr>                          
                            @endforelse                           
                            </tbody>
                        </table>
                    </div>


@endsection
@section('scripts')
<script type="text/javascript">
           
            function update(id){
                //alert(id);
                  $("input[name='user_id']").val(id);
                $('#editModal').modal('show');
            }
            
            
           
       </script>
@endsection