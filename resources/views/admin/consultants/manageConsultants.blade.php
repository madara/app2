@extends('admin.master')
@section('content')

                    <div class="card">
                        <div class="card-header">
                            <h2>System Consultants
                                <small>Consultants and their assigned Clients
                                </small>
                            </h2>
                        </div>

     <table  class="table table-striped table-vmiddle" id="client">
                            <thead>
                            <tr>
                                
                                <th style="width: 140px;">Consultant</th>
                                <th>Phone</th> 
                                <th>Location</th>                             
                                <th>Client Name</th>
                                <th>Client Phone</th>
                                <th>Client Location</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($clients as $key=>$client)
                            <tr>
                               
                                <td>{{$client->c_first_name}}{{' '}}{{$client->c_second_name}}</td>                             
                                <td>{{$client->c_phone}}</td> 
                                <td>{{$client->c_location}}</td> 
                                <td>{{$client->first_name}}{{' '}}{{$client->second_name}}</td> 
                                <td>{{$client->phone}}</td>
                                <td>{{$client->location}}</td>                                                           
                                <td>
                                <button style="color: #00BCD4" type="button" onclick="return viewClients('{{$client->user_id}}') " class="btn btn-icon command-edit waves-effect waves-circle " ><span class="zmdi zmdi-eye" ></span></button>                                
                                <button style="color: red" type="button" class="btn btn-icon command-delete waves-effect waves-circle delete-btn" onclick="return deleteUser('{{$client->user_id}}')" ><span class="zmdi zmdi-close"></span></button>
                                </td>
                                <form action="{{route('revokeConsultant', $client->user_id)}}" style="visibility: hidden;" id="{{$client->user_id}}" method='POST' >                                
                                 {{csrf_field()}}                                 
          
                                </form>
                                
                                
                                
                            </tr> 
                            @empty 
                             <tr>
                            <td colspan="7" style="text-align: center; color: #03A9F4;">No Consultant is assigned a client at the moment</td></tr>  
                            @endforelse                          
                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">More Client Details</h4>
                                        </div>
                                        <div class="modal-body">
                                     <form action="{{route('removeClient')}}" id="submit" method='POST' > 
                                        {{csrf_field()}}                                         
                                     <table class="table table-striped table-vmiddle">
                                         <thead>
                                             <tr>                                                
                                                 <th>Name</th>
                                                 <th>Email</th>
                                                 <th>Phone</th>
                                                 <th>Location</th>
                                                 <th>Paid</th>
                                                 <th>Remaining</th>
                                                 <th>Action</th>

                                             </tr>
                                         </thead>
                                         <tbody>                                        
                                             <tr>
                                                
                                                 <td id="name"></td>
                                                 <td id="email"></td>
                                                 <td id="phone"></td>
                                                 <td id="location"></td>
                                                 <td id="paid"></td>
                                                 <td id="months"></td>
                                                  <input type="hidden" name="consultant_id" id="consultant_id">
                                                 <input type="hidden" name="user_id" id="user_id">   
                                                 <td><button style="color: red" type="button" class="btn btn-icon command-delete waves-effect waves-circle delete-btn" onclick="return removeClient()" ><span class="zmdi zmdi-close"></span></button></td>
                                               
                                             </tr>
                                        
                                         </tbody>
                                     </table>
                                  </form>
                                                                                       
                                    <div class="modal-footer">                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                                            
                                        </div>
                                        </div>
                                       
                                    </div>
                                </div>
                    </div>   
      

@endsection
@section('scripts')

<script type="text/javascript">

     function deleteUser(id){

               // alert(id);
                swal({
                title: "Are you sure?",
                text: "You are about to unassign a client!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "unassign!",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {   

                        document.getElementById(id).submit();
                   
                }
            });
            
     }
    
            function viewClients(id){
                //alert(id);
                  //$("input[name='id']").val(id);
                $.get("{{route('getClientDetails')}}", {id:id}, function(data){
                    console.log(data);
                    //alert(data.first_name);
                    $('#name').text(data.first_name + ' ' + data.second_name);
                    $('#phone').text(data.phone);
                    $('#email').text(data.email);
                    $('#location').text(data.location);
                    $('#paid').text(data.amount);
                    $('#consultant_id').val(data.consultant_id);
                    $('#user_id').val(data.user_id);
                    var days=data.days;
                    var date=moment(data.created_at);                     
                    var now=new Date();
                       var edited=moment(now);
                       var duration=edited-date;
                    //var remaining=now-date2;
                    var daysPassed=moment.duration(duration).days();
                    $('#months').text( days-daysPassed +' days');
                    $('#editModal').modal('show');
                })
                
            }

            function removeClient(){
                //alert(id);
                swal({
                title: "Are you sure?",
                text: "You will not be able to recover this user",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, unassign!",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {   

                        document.getElementById('submit').submit();
                   
                }
            });
            }
            function disapprove(id, username){
        //alert(username)
            swal({
                title: "Unassign Client?",
                text: "You are about to unassign a Client",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, unassign!",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {   
                               
                        document.getElementById(username).submit();
                   
                }
            });
    }

$(window).on('load',function(){
    mergeCommonRows($('#client'));

    function mergeCommonRows(table){
    //console.log(table);
    //alert('here');
    var firstColumnBrakes=[];
    $.each(table.find('th'), function(i){
        var previous=null, cellToExtend=null,rowspan=1;
        table.find("td:nth-child("+ i + ")").each(function(index, e){ 
            var jthis=$(this), content= jthis.text();
            if(previous==content && content !== "" && $.inArray(index,firstColumnBrakes)=== -1){
                jthis.addClass('hidden');
                cellToExtend.attr("rowspan", (rowspan= rowspan+1));
            }
            else{
                if(i=== 1) firstColumnBrakes.push(index);
                rowspan=1;
                previous=content;
                cellToExtend=jthis;
            }
        });
    });
    //$('td.hidden').remove();
}
});


       </script>
@endsection