@extends('admin.master')
@section('content')

                    <div class="card">
                        <div class="card-header">
                            <h2>Users to Consultants
                                <small>Assign Clients To Consultants
                                </small>
                            </h2>
                            @if(Session::has('message'))
                            <p class="alert alert-danger">{{ Session::get('message') }}</p>
                            @endif
                        </div>

                        <table id="data-table-client" class="table table-striped table-vmiddle">
                            <thead>
                            <tr>
                                <th data-column-id="id" data-type="numeric" data-order="asc">#</th>
                                <th data-column-id="name" >Name</th>
                                <th data-column-id="email" data-order="desc">Email</th>
                                <th data-column-id="phone" data-order="desc">Phone</th>
                                <th data-column-id="location" data-order="desc">Location</th>   
                               <th data-column-id="payment" data-order="desc">Payment</th>
                               <th data-column-id="payment" data-order="desc">Amount</th>
                               <th data-column-id="payment" data-order="desc">Days</th>
                             
                                <th data-column-id="commands"  data-sortable="false" >Commands
                                </th>
                            </tr>
                            </thead>
                            <tbody>                          
                            @forelse($users as $key=>$user)                           
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$user->first_name}}{{' '}}{{$user->second_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone}}</td>                                
                                <td>{{$user->location}}</td>                                
                                <td>Paid</td>
                                <td>{{$user->amount}}</td>
                                <td>{{$user->days}} days</td>
                            <!--     <td hidden="hidden">{{$user->id}}</td> -->
                                <td>                                
                                <button  type="button" class="btn btn-success btn-sm " onclick="return update('{{$user->id}}')" ><span class="zmdi zmdi-check"></span>Assign</button>
                                <form action="{{route('deleteUser', $user->id)}}" style="visibility: hidden;" id="{{$user->id}}" method='POST' >                                
                                 {{csrf_field()}}                                 
          
                                </form>
                                </td>
                            </tr> 
                            @empty
                            <tr>
                            <td colspan="9" style="text-align: center; color: #03A9F4;">No New Clients at the moment</td></tr>                          
                            @endforelse                           
                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Assign Clients to Consultants</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- content goes here -->
                                          <form action="{{route('assign')}}" method="POST">
                                          {{csrf_field()}}  
                                               <div class="form-group">
                                                    <label for="id">Consultant</label>
                                                    <select name="id" class="form-control selectpicker" required="required" id="consul">
                                                    <option value="" selected="selected">------------------</option>
                                                    @foreach($consuls as $consul)
                                                        <option value="{{$consul->id}}">{{$consul->first_name}}{{' '}}{{$consul->second_name}}
                                                        </option>
                                                    @endforeach
                                                                                                             
                                                    </select>
                                             </div>      
                                           <div class="form-group">
                                                    <label for="location">Location</label>
                                                    <input type="text" name="location" id="location" disabled="disabled" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                    <label for="phone">Phone</label>
                                                    <input type="text" name="phone" id="phone" disabled="disabled" class="form-control">
                                            </div>  
                                            <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="text" name="email" id="email" disabled="disabled" class="form-control">
                                            </div> 
                                            <div class="form-group">
                                                    <label for="clients">Clients</label>
                                                    <input type="clients" name="clients" id="clients" disabled="disabled" class="form-control">
                                            </div>  
                                          <!--  --> 
                                          <input type="hidden" name="user_id" id="user_id">           
                                        
                                             


                                            <div class="modal-footer">
                                                             
                                                        <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Assign</button>
                                                   
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                                            
                                            </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
 </div>   
      

@endsection
@section('scripts')
<script type="text/javascript">
           
            function update(id){
                //alert(id);
                  $("input[name='user_id']").val(id);
                $('#editModal').modal('show');
            }
            $("#consul").on('change', function(){
                //alert($(this).val());
                var id=$(this).val();


                $.get("{{route('showConsultantInfo')}}", {id:id}, function(data){
                   // console.log(data);

                   $('#location').val(data.location);
                   $('#phone').val(data.phone);                   
                   $('#email').val(data.email);
                   $('#clients').val(data.total_clients)
                   //$('#id').val(data.id);

                })

            });
            
           
       </script>
@endsection