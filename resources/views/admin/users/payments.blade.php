@extends('admin.master')
@section('content')

 <div class="card">
                        <div class="card-header">
                            <h2>Payments                            
                            </h2>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Receipt</th>
                                    <th>Amount</th>
                                    <th>Days</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $key=>$payment)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$payment->user->first_name}}{{' '}}{{$payment->user->second_name}}</td>
                                    <td>{{$payment->created_at->toDateString()}}</td>
                                    <td>{{$payment->receipt}}</td>
                                    <td>{{$payment->amount}}</td>
                                    <td>{{$payment->days}}</td>
                                    @if($payment->status==1)
                                    <td><button class="btn btn-success btn-xs" onclick="return disapprove('{{$payment->id}}','{{$payment->receipt}}')"><i class="zmdi zmdi-check-all"></i>Approved </button></td>
                                    @else
                                    <td><button class="btn btn-warning btn-xs" onclick="return approve('{{$payment->id}}')">Pending</button></td>
                                    @endif
                                    <td>
                                    <button style="color: #00BCD4" type="button" onclick="#" class="btn btn-icon command-edit waves-effect waves-circle " ><span class="zmdi zmdi-eye" ></span></button>
                                    <button style="color: #FFC107" type="button" onclick="return editPayment('{{$payment->id}}','{{$payment->receipt}}')" class="btn btn-icon command-edit waves-effect waves-circle edit-btn" ><span class="zmdi zmdi-edit" ></span></button>
                                    <button style="color: #4CAF50" type="button" class="btn btn-icon command-delete waves-effect waves-circle delete-btn"  ><span class="zmdi zmdi-check-all"></span></button></td>
                                    <form action="{{route('approve', $payment->id)}}" style="visibility: hidden;" id="{{$payment->id}}" method='POST' >                                
                                     {{csrf_field()}}                                 
              
                                    </form>
                                    <form action="{{route('disapprove', $payment->id)}}" style="visibility: hidden;" id="{{$payment->receipt}}" method='POST' >                                
                                     {{csrf_field()}}                                 
              
                                    </form>
                                </tr> 
                                @endforeach
                                </tbody>
                            </table>
                        </div>
 </div>
 <div class="modal fade" id="editPayment" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Edit Payment</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- content goes here -->
                                          <form action="{{route('editPayment')}}" method="POST">
                                          {{csrf_field()}}       
                                           <div class="form-group">
                                                    <label for="role">Receipt</label>
                                                    <input type="text" name="receipt" class="form-control">
                                            </div> 
                                            <input type="hidden" name="id">             
                                             <div class="form-group">
                                                    <label for="status">Status</label>
                                                    <select name="status" class="form-control" required="required">
                                                       
                                                        <option value="1" selected="selected">Approve</option>
                                                        <option value="0">Disapprove</option>                                                       
                                                    </select>
                                             </div> 
                                             


                                            <div class="modal-footer">
                                                             
                                                        <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Update</button>
                                                   
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                                            
                                            </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
 </div> 
@endsection
@section('scripts')
<script src="{{URL::to('js/jquery.js')}}"></script>
<script type="text/javascript">
    function approve(id){
       // alert(id);
         document.getElementById(id).submit();
    }
    function disapprove(id, rec){
        //alert(rec)
            swal({
                title: "Disapprove Payment?",
                text: "You are about to disapprove payment!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, disapprove!",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {   
                               
                        document.getElementById(rec).submit();
                   
                }
            });
    }
    function editPayment(id,rec){
        $("input[name='id']").val(id);
         $("input[name='receipt']").val(rec);
        $('#editPayment').modal('show');
    }
</script>
@endsection