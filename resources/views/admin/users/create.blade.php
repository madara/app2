@extends('admin.master');
@section('content')
<div class="card">
    <div class="card-header">
        <h2>Create Users
            <small>Create Members </small>
        </h2>
    </div>
    <div class="card-body card-padding">
        <div class="form-group ">
            <form action="{{route('saveUser')}}" method="POST"  id="createUserForm">
                {{csrf_field()}}
                <div class="lcb-form">
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control" placeholder="First Name" name="first_name">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control" placeholder="Second Name" name="second_name">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class=" zmdi zmdi-pin-account"></i></span>
                        <div class="fg-line{{$errors->has('username') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Username" name="username" required="required">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-phone"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control input-mask" data-mask="+254700000000" placeholder="phone" name="phone">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                        <div class="fg-line">
                            <input type="email" class="form-control " placeholder="email" name="email">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-balance-wallet"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control input-mask"  placeholder="ID number" data-mask="00000-000" name="id_no">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-pin-drop"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control" placeholder="Location" name="location">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-male-female"></i></span>
                        <label class="radio radio-inline m-r-20">
                            <input type="radio" name="gender" value="Male" >
                            <i class="input-helper"></i>
                            Male
                        </label>

                        <label class="radio radio-inline m-r-20">
                            <input type="radio" name="gender" value="Female" >
                            <i class="input-helper"></i>
                            Female
                        </label>
                    </div>

                </div>
                <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button onclick="return submitfun()" href="" class="btn btn-login btn-success " type="submit"><i class="zmdi zmdi-save"></i>Save</button></div>

            </form>
        </div>

    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
       function submitfun() {
        $('#createUserForm').submit();
       }
    </script>
@endsection