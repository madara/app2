@extends('admin.master')
@section('content')

                    <div class="card">
                        <div class="card-header">
                            <h2>System Users
                                <small>Both Consultants and Members
                                </small>
                            </h2>
                            @if(Session::has('message'))
                            <p class="alert alert-danger">{{ Session::get('message') }}</p>
                            @endif
                        </div>

     <table  class="table table-striped table-vmiddle">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Commands
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key=>$user)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$user->first_name}}{{' '}}{{$user->second_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->role_name}}</td>
                                @if($user->active==1)
                                <td><button class="btn btn-success btn-xs" onclick="return disapprove('{{$user->id}}','{{$user->username}}')"><i class="zmdi zmdi-check-all"></i>Activated</button> </td>
                                @else
                                <td><button class="btn btn-warning btn-xs " onclick="return approveUser('{{$user->id}}')">Pending</button> </td>
                                @endif
                                <td>{{$user->created_at->diffForHumans()}}</td>
                                <td>
                                <button style="color: #00BCD4" type="button" onclick="#" class="btn btn-icon command-edit waves-effect waves-circle " ><span class="zmdi zmdi-eye" ></span></button>
                                <button style="color: #00BCD4" type="button" onclick="return update('{{$user->id}}')" class="btn btn-icon command-edit waves-effect waves-circle edit-btn" ><span class="zmdi zmdi-edit" ></span></button>
                                <button style="color: red" type="button" class="btn btn-icon command-delete waves-effect waves-circle delete-btn" onclick="return deleteUser('{{$user->id}}')" ><span class="zmdi zmdi-delete"></span></button>
                                <form action="{{route('deleteUser', $user->id)}}" style="visibility: hidden;" id="{{$user->id}}" method='POST' >                                
                                 {{csrf_field()}}                                 
          
                                </form>
                                <form action="{{route('approveUser', $user->id)}}" style="visibility: hidden;" id="{{$user->id}}" method='POST' >                                
                                     {{csrf_field()}}                                 
              
                                </form>
                                <form action="{{route('disapproveUser', $user->id)}}" style="visibility: hidden;" id="{{$user->username}}" method='POST' >                                
                                     {{csrf_field()}}                                 
              
                                    </form>
                                </td>
                                
                            </tr> 
                            @endforeach                           
                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Edit User</h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- content goes here -->
                                          <form action="{{route('updateUser')}}" method="POST">
                                          {{csrf_field()}}       
                                           <div class="form-group">
                                                    <label for="role">Role</label>
                                                    <select name="role_id" class=" form-control selectpicker" required="required">
                                                    <option value="" selected="selected">------------</option>
                                                        @foreach($roles as $role )
                                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                                        @endforeach 
                                                    </select>
                                            </div> 
                                            <input type="hidden" name="id">             
                                             <div class="form-group">
                                                    <label for="status">Status</label>
                                                    <select name="active" class="form-control selectpicker" required="required">
                                                       
                                                        <option value="1" selected="selected">Activate</option>
                                                        <option value="0">Deactivate</option>                                                       
                                                    </select>
                                             </div> 
                                             


                                            <div class="modal-footer">
                                                             
                                                        <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Update</button>
                                                   
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                                            
                                            </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
 </div>   
      

@endsection
@section('scripts')

<script type="text/javascript">

           
    function approveUser(id){
       // alert(id);
         document.getElementById(id).submit();
    }
            function update(id){
                //alert(id);
                  $("input[name='id']").val(id);
                $('#editModal').modal('show');
            }

            function deleteUser(id){
                //alert(id);
                swal({
                title: "Are you sure?",
                text: "You will not be able to recover this User!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete !",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {   

                        document.getElementById(id).submit();
                   
                }
            });
            }
            function disapprove(id, username){
        //alert(username)
            swal({
                title: "Deactivate user?",
                text: "You are about to diactivate a user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, diactivate!",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {   
                               
                        document.getElementById(username).submit();
                   
                }
            });
    }




       </script>
@endsection