            <aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a href="" data-ma-action="profile-menu-toggle">
                        <div class="sp-pic">
                            <img src="{{url('img/profile-pics/1.jpg')}}" alt="">
                        </div>

                        <div class="sp-info">
                            {{Auth::user()->first_name}} {{Auth::user()->second_name}}

                            <i class="zmdi zmdi-caret-down"></i>
                        </div>
                    </a>

                    <ul class="main-menu">
                        <li>
                            <a href="profile-about.html"><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>                        
                        <li>
                            <a href="{{route('logout')}}"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                        </li>
                    </ul>
                </div>

                <ul class="main-menu">
                    <li class="active">
                        <a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>

                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-accounts"></i> Users</a>

                        <ul>
                            <li><a href="{{route('createUsers')}}">Create</a> </li>
                            <li><a href="{{route('manageUsers')}}">Manage</a></li>

                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-money-box"></i> Payments</a>

                        <ul>
                            <li><a href="{{route('approvePayments')}}">Approve</a></li>                            
                        </ul>
                    </li>  
                    
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-assignment-account"></i> Consultants</a>
     
                        <ul>
                            <li><a href="{{route('createConsultant')}}">Create</a> </li>
                            <li><a href="{{route('assignClients')}}">Assign Clients</a></li>
                            <li><a href="{{route('manageConsultants')}}">Manage</a></li>
                        </ul>
                        
                    </li>                             
                      
                </ul>
            </aside>