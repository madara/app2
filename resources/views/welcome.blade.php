<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>login</title>

        <!-- Vendor CSS -->
        <link href="{{URL::to('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
        <link href="{{URL::to('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">

        <!-- CSS -->
        <link href="{{URL::to('css/app_1.min.css')}}" rel="stylesheet">
        <link href="{{URL::to('css/app_2.min.css')}}" rel="stylesheet">
        <link href="{{URL::to('vendors/farbtastic/farbtastic.css')}}" rel="stylesheet">
        <link href="{{URL::to('vendors/bower_components/chosen/chosen.css')}}" rel="stylesheet">
    </head>
    <body>
       <form action="{{route('login')}}" method="POST">         
            {!!csrf_field()!!} 
        <div class="login-content">
            <!-- Login -->
       
            <div class="lc-block toggled" id="l-login">
                <div class="lcb-form">
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line{{$errors->has('username') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Username" name="username" required="required">
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>

                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                        <div class="fg-line{{$errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="required">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">
                            <i class="input-helper"></i>
                            Keep me signed in
                        </label>
                    </div>

                    <button class="btn btn-login btn-success btn-float" id="login" type="submit"><i class="zmdi zmdi-arrow-forward"></i></button>
                </div>

                <div class="lcb-navigation">
                    <a href="" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>
                    <a href="" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
                </div>
            </div>
        </div>
       </form>

            <!-- Register -->
       <div class="lc-block" id="l-register">
            <form action="{{route('register')}}" method="POST" >
            {{csrf_field()}}
                <div class="lcb-form">
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control" placeholder="First Name" name="first_name">
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control" placeholder="Second Name" name="second_name">
                        </div>
                    </div>
                     <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class=" zmdi zmdi-pin-account"></i></span>
                        <div class="fg-line{{$errors->has('username') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Username" name="username" required="required">
                            @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="role_id">
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                        <div class="fg-line{{$errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Email Address" name="email" required="required">
                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                        <div class="fg-line">
                            <input type="password" class="form-control" placeholder="Password" name="password" required="required">
                        </div>
                    </div>

                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                        <div class="fg-line">
                            <input type="password" class="form-control" placeholder="confirm password"  name="password_confirmation" required>
                        </div>
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-phone"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control input-mask" data-mask="(00) 0000-0000" placeholder="phone" name="phone">
                        </div>
                    </div>
                     <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-balance-wallet"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control input-mask"  placeholder="ID number" data-mask="00000-000" name="id_no">
                        </div>
                    </div>
                     <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-pin-drop"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control" placeholder="Location" name="location">
                        </div>
                    </div>
                     <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-male-female"></i></span>
                            <label class="radio radio-inline m-r-20">
                               <input type="radio" name="gender" value="Male" >
                                     <i class="input-helper"></i>
                                    Male                      
                             </label>

                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="gender" value="Female" >
                                     <i class="input-helper"></i>
                                Female
                            </label>
                                
                            
                    </div>
                    <div class="input-group m-b-20">
                        <span class="input-group-addon">Consultant?</span>
                            <label class="radio radio-inline m-r-20" >
                               <input type="radio" name="consultant" value="yes" required>
                                     <i class="input-helper"></i>
                                    Yes                      
                             </label>

                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="consultant" value="no" required>
                                     <i class="input-helper"></i>
                                No
                            </label>
                                
                            
                    </div>




                    <button href="" class="btn btn-login btn-success btn-float" type="submit"><i class="zmdi zmdi-check"></i></button>
                </div>

                <div class="lcb-navigation">
                    <a href="" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
                    <a href="" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
                </div>
            </form>
       </div>
            <!-- Forgot Password -->
            <div class="lc-block" id="l-forget-password">
                <div class="lcb-form">
                    <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu risus. Curabitur commodo lorem fringilla enim feugiat commodo sed ac lacus.</p>

                    <div class="input-group m-b-20">
                        <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                        <div class="fg-line">
                            <input type="text" class="form-control" placeholder="Email Address">
                        </div>
                    </div>

                    <a href="" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-check"></i></a>
                </div>

                <div class="lcb-navigation">
                    <a href="" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
                    <a href="" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>
                </div>
            </div>
        </div>
        <!-- Javascript Libraries -->
        <script src="{{URL::to('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{URL::to('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

        <script src="{{URL::to('vendors/bower_components/Waves/dist/waves.min.js')}}"></script>
        <script src="{{URL::to('js/app.min.js')}}"></script>
        <script src="{{URL::to('vendors/bower_components/chosen/chosen.jquery.js')}}"></script>
        <script src="{{URL::to('vendors/fileinput/fileinput.min.js')}}"></script>
        <script src="{{URL::to('vendors/input-mask/input-mask.min.js')}}"></script>
        <script src="{{URL::to('vendors/farbtastic/farbtastic.min.js')}}"></script>
    </body>
</html>