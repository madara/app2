<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $fillable=['amount', 'days', 'receipt',  'user_id', 'status', 'total', 'payed'];
    public $timestamps=true;


    public function user(){
   	return $this->belongsTo(User::class);
   }
}
