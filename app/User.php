<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','second_name', 'email', 'password','role_id','username', 'active', 'phone','id_no', 'location', 'gender', 'photo', 'consultant_id', 'revoked'
    ];
    public $timestamps=true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function role(){
        return $this->hasOne('App\Role','id','role_id');
    }
    public function consultant(){
        return $this->hasOne('App\Consultant','id','consultant_id');
    }
    private function CheckIfUserHasRole($need_role){
        //dd($this->role->name);
        //dd(Auth::user()->email);
        //dd($need_role);
        return (strtolower($need_role)==strtolower($this->role->name))? true : null;
    }
    public function hasRole($roles){
        //dd($roles);
        if (is_array($roles)){
            foreach ($roles as $need_role) {
                if($this->CheckIfUserHasRole($need_role)){
                    //dd($need_role);
                    return true;
                }
            }
        }else
        {
            return $this->CheckIfUserHasRole($roles);
        }
        return false;
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }
}
