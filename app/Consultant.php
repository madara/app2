<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultant extends Model
{
    //
   protected $fillable=['first_name','second_name', 'email', 'password','role_id','username', 'active', 'phone','id_no', 'location', 'gender', 'photo', 'total_clients'];
   public $timestamps=true;
   protected $hidden = [
        'password', 'remember_token',
    ];
    public function users(){
    	 return $this->hasMany('App\User', 'consultant_id');
    }
}
