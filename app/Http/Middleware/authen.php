<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class authen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard ='web')
    {
        if(!Auth::guard($guard)->check()){
            //dd('success');
            return redirect()->route('/');
        }
        return $next($request);

    }
}
