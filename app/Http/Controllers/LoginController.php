<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Auth;
class LoginController extends Controller
{
    //

    use AuthenticatesUsers;   
    protected $redirectTo = '/dashboard';
    protected $username= 'username';
    protected $guard ='web';

    public function getlogin(){
    	//dd('here');
    	if(Auth::guard('web')->check()){
    		return redirect()->route('dashboard');
    	}
    	return view('welcome');
    }

    public function postlogin(Request $request){
    	
		//dd('here');
        //dd($request->all());
    	$auth= Auth::guard('web')->attempt(['username'=>$request->username,'password'=>$request->password]);
        $auth2= Auth::guard('web')->attempt(['username'=>$request->username,'password'=>$request->password, 'active'=>1]);
    	if($auth){    		
            if($auth2){
                if(Auth::user()->role->name=='user'){
                    return redirect()->route('user.index');
                }
                if(Auth::user()->role->name=='consultant'){
                    return redirect()->route('consultant.index');
                }
                else{
                      return redirect()->route('dashboard');
                }
             
            }
            else{
               // dd('here');
               return back()->withErrors(['username'=>['Your account has been deactivated, please contact the adminstrator']]);
            }
    	}
    	return redirect()->route('/')->withErrors(['username'=>['Invalid username or Password']]);
    }
    
    public function getlogout(){
    	Auth::guard('web')->logout();
    	return redirect()->route('/');
    }
}
