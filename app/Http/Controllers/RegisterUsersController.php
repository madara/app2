<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Consultant;
use Auth;

class RegisterUsersController extends Controller
{

    //
    use RegistersUsers;
    protected $redirectTo = '/dashboard';
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request){
    	//dd($request->all());

    	$this->validate($request, ['first_name'=>'required', 'second_name'=>'required' ,'email'=>'required|unique:users', 'password'=>'required|min:6|confirmed']);
      
            //dd('user');
         if($request->consultant=='no'){
          User::create([
            'first_name'=>$request->first_name,
            'second_name'=>$request->second_name,
            'username' => $request->username,
            'role_id'=>4,
            'active'=>1,
            
            'email' => $request->email,
            'phone'=>$request->phone,
            'id_no'=>$request->id_no,
            'location'=>$request->location,
            'gender'=>$request->gender,
            'password' => bcrypt($request->password),

        ]); 
      }
      
        if ($request->consultant=='yes'){

            User::create([
            'first_name'=>$request->first_name,
            'second_name'=>$request->second_name,
            'username' => $request->username,
            'role_id'=>2,
            'active'=>1,
            
            'email' => $request->email,
            'phone'=>$request->phone,
            'id_no'=>$request->id_no,
            'location'=>$request->location,
            'gender'=>$request->gender,
            'password' => bcrypt($request->password),
        ]); 
            //dd('cons');
          Consultant::create([
            'first_name'=>$request->first_name,
            'second_name'=>$request->second_name,
              'role_id'=>2,
            //'active'=>1,
            'email' => $request->email,
            'phone'=>$request->phone,
            'id_no'=>$request->id_no,
            'location'=>$request->location,
            'gender'=>$request->gender,

        ]);   
        
        }
    	  
    	  return redirect()->route('/');
    }
}
