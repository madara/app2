<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
Use App\Payment;
use App\User;
use App\Role;
use App\Consultant;
class AdminController extends Controller
{
    //
     public function __construct(){
    	$this->middleware('web');
    }
    public function dashboard(){
    	return view('admin.home');
    }
    public function manageUsers(){
    	$id=Auth::user()->id;
    	//dd($id);
    	$users=User::join('roles', 'roles.id', 'users.role_id')
    				->select('users.id', 'users.first_name as first_name', 'users.second_name', 'users.email', 'users.username', 'users.active','roles.name as role_name', 'users.created_at')
    				->where('users.id', '!=', $id)
    				->get();
    	//dd($users);
    	$roles=Role::all();
    	//dd($roles);
    	return view('admin.users.manageUsers', compact(['users', 'roles']));

    }
    public function updateUser(Request $request){

       //dd($request->all());
    	$user=User::findorfail($request->id);
       // dd($user->id);
        $consultant=Consultant::where('email',$user->email)->first();
        //dd($consultant);
       $payment=Payment::where('user_id', $user->id)->first();
      // dd($payment);
        if($consultant==null){
            if($payment!= null){
              $payment->delete();
              $user->update(['consultant_id'=>null]);
            }
           
            Consultant::create(['role_id'=>2, 'first_name'=>$user->first_name, 'second_name'=>$user->second_name, 'gender'=>$user->gender,  'email'=>$user->email, 'phone'=>$user->phone, 'location'=>$user->location, 'id_no'=>$user->id_no]);

        }      
        
        if($request->role_id==4 && $consultant !=null){
           $cons=Consultant::where('email', $user->email)->first();
          // dd('here');
          // dd($cons);
            $cons->delete();
           $user->update(['consultant_id'=>null]);

        }

    	//dd($user);
    	//dd($request->all());
    	//if($request->role_id)
    	$user->update(['role_id'=>$request->role_id, 'active'=>$request->active]);

    	
    	return back();

    }
    public function deleteUser($id){
    	//dd($id);
    	$user=User::findorfail($id);
    	//dd($user);
    	//dd($user);
    	$user->delete();

    	return back();
    }
    public function approvePayments(){
    	$payments= Payment::all();
    	//dd($payments);
    	return view('admin.users.payments',compact('payments'));
    }
    public function approve($id){
    	//dd('here');
    	$payment=Payment::findorfail($id);
    	$payment->update(['status'=>1]);
    	return back();
    }
    public function disapprove($id){

    	//dd($id);
    	$payment=Payment::findorfail($id);
    	//dd($payment);
    	$payment->update(['status'=>0]);
    	return back();
    }
    public function editPayment(Request $request){
    	//dd('here');
    	//dd($request->all());
    	$payment=Payment::findorfail($request->id);
    	$payment->update(['receipt'=>$request->receipt, 'status'=>$request->status]);
    	return back();
    }
    public function assignClients(){

        $consultants=User::Join('consultants','consultants.id', 'users.consultant_id')
                            ->select('consultants.first_name as c_first_name', 'consultants.second_name as c_second_name', 'consultants.location as c_location')
                            ->get();
        //dd($consuls);
        $users=User::join('payments', 'users.id', 'payments.user_id')
                    //->join('consultants','consultants.id', 'users.consultant_id')
                    ->select('users.id','users.first_name', 'users.second_name','payments.amount', 'payments.days', 'users.email', 'users.location', 'users.phone', 'users.consultant_id', 'users.role_id', 'payments.status', 'users.revoked')
                    ->where([['users.consultant_id', null], ['role_id', 4], ['payments.status',1],['payments.update', 0]])
                    ->get() ; 

        $consuls=Consultant::all();
        return view('admin.consultants.AssignClients', compact(['users', 'consuls']));
    }

    public function showConsultantInfo(Request $request){
        if($request->ajax()){
            $con=Consultant::all();
          $consult= Consultant::where('id', $request->id)->first();
           //return response($consult);
            return $consult;
            }
        
    }
   public function assign(Request $request){
  // dd($request->all());
    $user_id=$request->user_id;
    $consultant_id=$request->id;
    $consultant=Consultant::where('id', $consultant_id)->first();
    //dd();

    //$clients=$consultant->total_clients;
    $clients=User::where('consultant_id', $consultant_id)->count();
    //dd($clients);

    $res=User::where('id', $user_id)->first();
   // dd($res);

    if($clients <=3){
        $res->update(['consultant_id'=>$consultant_id, 'revoked'=>0]);
         $consultant->update(['total_clients'=> $clients+1]);

    }
    else{
        return back()->with('message', 'The selected Consultant has reached a maximum number of clients');
    }
        
   
    //dd($res);
    return back();


   }
   public function approveUser($id){
    //dd($id);
    $user=User::findorfail($id);
        $user->update(['active'=>1]);
        return back();
   }
   public function disapproveUser($id){

        //dd($id);
        $user=User::findorfail($id);
        //dd($payment);
        $user->update(['active'=>0]);
        return back();
    }

    public function manageConsultants(){
        //$consultants=Consultant::all();
        $clients=Consultant::join('users', 'users.consultant_id', 'consultants.id')
                            //->join('payments','users.id', 'payments.user_id')
                            ->select('users.first_name', 'users.second_name','users.email', 'users.location', 'users.id as user_id', 'consultants.id as consultant_id' ,'users.phone', 'consultants.first_name as c_first_name', 'consultants.second_name as c_second_name', 'consultants.email as c_email' ,'consultants.total_clients', 'consultants.location as c_location', 'consultants.phone as c_phone')
                            ->get();
        //dd($clients);

        return view('admin.consultants.manageConsultants', compact('clients'));
    }
    public function getClientDetails(Request $request){

        if($request->ajax()){
            //$client=User::where('id', $request->id)->first();
            $client=User::join('payments', 'users.id', 'payments.user_id')
                            ->select('users.id as user_id', 'payments.id as payment_id', 'users.phone', 'users.first_name', 'users.second_name', 'users.location', 'payments.amount', 'payments.total', 'payments.days', 'users.consultant_id')
                            ->where('users.id', $request->id)
                            ->orderBy('payment_id','desc')
                            ->first();

            return $client;
        }
    }
    public function removeClient(Request $request){
      //dd($request->all());
        $consultant=Consultant::where('id', $request->consultant_id)->first();
        //dd($consultant);
        $totalclients=$consultant->total_clients;       
        $consultant->update(['total_clients'=>$totalclients-1]);
        $user=User::where('id', $request->user_id)->first();
        $user->update(['consultant_id' => null, 'revoked'=>1]);
        return back();

    }
    public function revokeConsultant(Request $request,$id){        
        //dd($request->all());
        //dd($id);
        $user=User::where('id', $id)->first();
       // dd($user->consultant_id);
        $consultant=Consultant::where('id', $user->consultant_id)->first();
       // dd($consultant->total_clients);
        $totalclients=$consultant->total_clients;
       // dd($totalclients-2);
        $consultant->update(['total_clients'=>$totalclients-1]);

        $user->update(['consultant_id' => null, 'revoked'=>1]);
        return back();

        //dd($user);

    }
    public function createUsers(){

        return view('admin.users.create');
    }
    public  function  saveUser(Request $request)
    {
        //dd([$request->all(), 'here']);
        $this->validate($request, ['first_name'=>'required', 'second_name'=>'required' , 'email'=>'required|unique:users']);
        User::create([
            'first_name'=>$request->first_name,
            'second_name'=>$request->second_name,
            'username' => $request->username,
            'role_id'=>4,
            'active'=>0,
            'passrec'=>0,
            'email' => $request->email,
            'phone'=>$request->phone,
            'id_no'=>$request->id_no,
            'location'=>$request->location,
            'gender'=>$request->gender,
            'password' => bcrypt('password'),

        ]);
        return redirect()->route('manageUsers');
    }
    public  function  createConsultants(){
        return view('admin.consultants.create');
    }
    public  function  saveConsultant(Request $request)
    {
        //dd([$request->all(), 'here']);
        $this->validate($request, ['first_name'=>'required', 'second_name'=>'required' , 'email'=>'required|unique:users']);
        User::create([
            'first_name'=>$request->first_name,
            'second_name'=>$request->second_name,
            'username' => $request->username,
            'role_id'=>2,
            'active'=>1,
            'passrec'=>0,
            'email' => $request->email,
            'phone'=>$request->phone,
            'id_no'=>$request->id_no,
            'location'=>$request->location,
            'gender'=>$request->gender,
            'password' => bcrypt('password'),

        ]);
        return redirect()->route('manageUsers');
    }
}
