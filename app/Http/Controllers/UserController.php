<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use Auth;
use Hash;
use App\User;
//use Alert;
use App\Consultant;
use Carbon\Carbon;
class UserController extends Controller
{
    //
    public function index(){
    	return view('user.home');
    }

    public function subscription(){

    	return view('user.subscription');
    }
    public function payment(Request  $request){
    	//dd($request->all());
       // $userPayment=Payment::where('user_id', Auth::user()->id)->first();
        $userPayment=Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        //dd($userPayment);       
        if($userPayment==null){
          Payment::create(['user_id'=>Auth::user()->id, 'amount'=>$request->amount, 'days'=>$request->days, 'receipt'=>$request->receipt, 'total'=>$request->amount]);   
        }
        if($userPayment!=null){
           $created=Carbon::parse($userPayment->created_at);           
           $now=Carbon::now();
           $length=$now->diffInDays($created);
           $updated_date=$request->days+$length;           
            $total=$userPayment->total+ $request->amount;
            $saving=$request->days+$userPayment->days-$length;
            //dd($total);
            $payed=$request->amount;
            Payment::create(['user_id'=>Auth::user()->id,'amount'=>$payed, 'days'=>$saving ,'total'=>$total, 'receipt'=>$request->receipt, 'status'=>0,'update', 1]);

        }
       

    	return redirect()->route('viewpayment');
    	
    }
    public function viewpayment(){
    	$user=Auth::user();
    	///dd($user->payments);	
    	$payments=$user->payments;
    	return view('user.payments', compact("payments"));
    }
    public function profile(){
        $user=Auth::user();
        //dd($user);
        return view('user.profile', compact('user'));
    }
     public function updateBasic(Request $request){
       // dd($request->all());
        $user=Auth::user();
        $user->update(['first_name'=>$request->first_name,'id_no'=>$request->id_no, 'second_name'=>$request->second_name, 'username'=>$request->username, 'gender'=>$request->gender]);

        return back();

    }
    public function updateContact(Request $request){
       // dd($request->all());
        $user=Auth::user();Route::get('/users/create', '');
        $user->update(['phone'=>$request->phone, 'email'=>$request->email, 'location'=>$request->location]);

        return back();

    }

    public function password(){

        return view('user.password');
    }
    public function changePassword(Request $request){
        //dd($request->all());
        //dd('here');
         // $user=Auth::user();
        //$user=findorfail($id);
         $user = User::find(Auth::id());
         //dd($user);
        $this->validate($request,[
            'old_password' => 'required',
            'password'=>'required|min:4',
            'password_confirmation'=>'required|same:password'
            ]);
       
        
        $hashedPassword=$user->password;       
        //dd($hashedPassword);
        $password=$request->old_password;           
        if (Hash::check($request->old_password, $hashedPassword)) {
            //Change the password   

            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();  
            //$user->passrec=0;
            $user->save();     
    }
    else{
        return back()
        ->with('message','The specified password does not match the database password');
    }
   // Alert::success('Password Changed Successfuly', 'Changed!');
    return redirect()->route('user.profile');
    }
    public function updatePassword(){

        return view('user.updatePassword');
    }
    public  function  firstLogin(Request $request){
        //dd('here');
        //dd($request->all());
        //dd('here');
        $user = User::find(Auth::id());
        //dd($user);
        $this->validate($request,[
            'password'=>'required|min:4',
            'password_confirmation'=>'required|same:password'
        ]);


        $hashedPassword=$user->password;
            //Change the password

            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            $user->passrec=1;
            $user->save();

        return redirect()->route('user.profile');

    }
}
