<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consultant;
use App\User;
use Auth;
use DB;
use Hash;
class ConsultantController extends Controller
{
    //

    public function index(){

    	return view('consultant.home');
    }
    public function profile(){
        $user=Auth::user();
        //dd($user);
        return view('consultant.profile', compact('user'));
    }
     public function updateBasic(Request $request){
     	$consultant=User::findorfail(Auth::user()->id);
     	$id=$consultant->id_no;    	
     	$cons=Consultant::where('id_no', $id)->first();
     	$cons->update(['first_name'=>$request->first_name, 'id_no'=>$request->id_no,'second_name'=>$request->second_name, 'gender'=>$request->gender]);
     	//dd($cons);
        //dd($request->all());
     	
        $user=Auth::user();
        $user->update(['first_name'=>$request->first_name, 'id_no'=>$request->id_no,'second_name'=>$request->second_name, 'username'=>$request->username, 'gender'=>$request->gender]);

        return back();

    }
    public function updateContact(Request $request){
       // dd($request->all());
    	$consultant=User::findorfail(Auth::user()->id);
    	$id=$consultant->id_no;
    	$cons=Consultant::where('id_no', $id)->first();
    	$cons->update(['phone'=>$request->phone, 'email'=>$request->email, 'location'=>$request->location]);
        $user=Auth::user();
        $user->update(['phone'=>$request->phone, 'email'=>$request->email, 'location'=>$request->location]);

        return back();

    }

    public function password(){

        return view('consultant.password');
    }
    public function changePassword(Request $request){
        //dd($request->all());
        //dd('here');
         // $user=Auth::user();
        //$user=findorfail($id);
         $user = User::find(Auth::id());
         //dd($user);
        $this->validate($request,[
            'old_password' => 'required',
            'password'=>'required|min:4',
            'password_confirmation'=>'required|same:password'
            ]);
       
        
        $hashedPassword=$user->password;       
        //dd($hashedPassword);
        $password=$request->old_password;           
        if (Hash::check($request->old_password, $hashedPassword)) {
            //Change the password   

            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();  
            //$user->passrec=0;
            $user->save();     
    }
    else{
        return back()
        ->with('message','The specified password does not match the database password');
    }
   // Alert::success('Password Changed Successfuly', 'Changed!');
    return redirect()->route('consultant.profile');
    }
    public function viewClients(){
        $user=Auth::user()->email;
        $consultant=Consultant::where('email',$user)->first();
        $consultant_id=$consultant->id;
        //dd(User::all(), $user);
        $clients=User::where('consultant_id', $consultant_id)->get();
        //dd($client);
        return view('consultant.allClients', compact('clients'));
    }
    public function updatePassword(){

        return view('consultant.updatePassword');
    }
    public  function  firstLogin(Request $request){
        //dd($request->all());
        //dd('here');
        $user = User::find(Auth::id());
        // dd($user);
        $this->validate($request,[
            'password'=>'required|min:4',
            'password_confirmation'=>'required|same:password'
        ]);


        $hashedPassword=$user->password;
        //Change the password

        $user->fill([
            'password' => Hash::make($request->password)
        ])->save();
        $user->passrec=1;
        $user->save();
        return redirect()->route('consultant.profile');

    }
}
