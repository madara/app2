<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->integer('consultant_id')->unsigned();
            $table->string('first_name')->nullable();;
            $table->string('second_name')->nullable();
            $table->string('email', 70)->unique();
            $table->string('username', 50)->unique();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('active')->nullable();
            $table->string('phone')->nullable();
            $table->integer('id_no')->nullable();
            $table->string('location')->nullable();
            $table->string('gender')->nullable();
            $table->string('photo')->nullable();
            $table->timestamps();
            $table->index('role_id');
            $table->index('consultant_id');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('consultant_id')->references('id')->on('consultants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
