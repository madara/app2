<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Consultant;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
       User::create([
    		'name'=>'admin ', 
            'admission_staff_no'=>'admin',           
    		'email'=>'admin@gmail.com',
            'role_id'=>1,
            'active'=>1,
            'consultant_id'=>1,
            'passrec'=>1,
            'password'=>bcrypt('admin'),
    		'remember_token'=> str_random(10),

    		]    		
    		);
    }
}
