<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/noPermission', function(){
	return view('nopermission');
})->name('nopermission');
Route::post('/register', 'RegisterUsersController@register')->name('register');
Route::get('/', ['as'=>'/', 'uses'=>'LoginController@getlogin']);
Route::post('/login', 'LoginController@postlogin')->name('login');
Route::get('/logout', 'LoginController@getlogout')->name('logout');

Route::group(['prefix'=>'admin', 'middleware'=> ['authen', 'roles'], 'roles'=>['admin']], function(){
	Route::get('/', ['as'=>'dashboard', 'uses'=>'AdminController@dashboard']);
	//Route::get('/logout', 'LoginController@getlogout')->name('logout');
	Route::get('/users/manage', 'AdminController@manageUsers')->name('manageUsers');
	Route::post('/users/update', 'AdminController@updateUser')->name('updateUser');
	Route::post('/users/delete/{id}', 'AdminController@deleteUser')->name('deleteUser');
	Route::get('/payments', 'AdminController@approvePayments')->name('approvePayments');
	Route::post('/approve/payments/{id}', 'AdminController@approve')->name('approve');
	Route::post('/disapprove/payments/{id}', 'AdminController@disapprove')->name('disapprove');
	Route::post('/edit/payments', 'AdminController@editPayment')->name('editPayment');
	Route::get('/consultants/clients', 'AdminController@assignClients')->name('assignClients');
	Route::get('/consultant/get', 'AdminController@showConsultantInfo')->name('showConsultantInfo');
	Route::post('/assign/clients', 'AdminController@assign')->name('assign');
	Route::post('/user/approve/{id}', 'AdminController@approveUser')->name('approveUser');
	Route::post('/user/disapprove/{id}', 'AdminController@disapproveUser')->name('disapproveUser');
	Route::get('/consultant/manage', 'AdminController@manageConsultants')->name('manageConsultants');
	Route::get('/client/details', 'AdminController@getClientDetails')->name('getClientDetails');
	Route::post('/client/remove', "AdminController@removeClient")->name('removeClient');
	Route::post('/client/revoke/consultant/{id}', 'AdminController@revokeConsultant')->name('revokeConsultant');
	Route::get('/users/create', 'AdminController@createUsers')->name('createUsers');
	Route::post('/users/save', 'AdminController@saveUser')->name('saveUser');
    Route::get('/consultant/create', 'AdminController@createConsultants')->name('createConsultant');
    Route::post('/consultant/save', 'AdminController@saveConsultant')->name('saveConsultant');
});


Route::group(['prefix'=>'member', 'middleware'=>['authen', 'roles'], 'roles'=>['user']], function(){
	Route::get('/', 'UserController@index')->name('user.index');
	Route::get('/subscription', 'UserController@subscription')->name('user.subscription');	
	Route::post('/payment', 'UserController@payment')->name('payment');
	Route::get('/payment/view', 'UserController@viewpayment')->name('viewpayment');
	Route::get('/user/profile', 'UserController@profile')->name('user.profile');
	Route::post('/update/basic', 'UserController@updateBasic')->name('updateBasic');
	Route::post('/update/contact', 'UserController@updateContact')->name('updateContact');
	Route::get('/update/password', 'UserController@password')->name('password');
	Route::post('/password/change', 'UserController@changePassword')->name('changePassword');
	Route::get('/profile/updatepassword', 'UserController@updatePassword')->name('updatePassword');
	Route::post('/profile/password/update', 'UserController@firstLogin')->name('user.firstLogin');

});
Route::group(['prefix'=>'consultant', 'middleware'=>['authen', 'roles'], 'roles'=>['consultant']], function(){
	Route::get('/', 'ConsultantController@index')->name('consultant.index');
	Route::get('/consultant/profile', 'ConsultantController@profile')->name('consultant.profile');
	Route::post('/update/basic', 'ConsultantController@updateBasic')->name('consultant.updateBasic');
	Route::post('/update/contact', 'ConsultantController@updateContact')->name('consultant.updateContact');
	Route::get('/update/password', 'ConsultantController@password')->name('consultant.password');
	Route::post('/password/change', 'ConsultantController@changePassword')->name('consultant.changePassword');
	Route::get('/clients', 'ConsultantController@viewClients')->name('allClients');
    Route::get('/profile/updatepassword', 'ConsultantController@updatePassword')->name('updatePassword');
    Route::post('/profile/password/update', 'ConsultantController@firstLogin')->name('consultant.firstLogin');

});



